ARG ANSIBLE_VERSION
FROM registry.gitlab.com/almaops/ct/ansible:${ANSIBLE_VERSION}
RUN mkdir /roles
COPY ./requirements.yml /requirements.yml
RUN ansible-galaxy install -r /requirements.yml -p /roles
